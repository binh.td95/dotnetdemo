﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ShopMail.Data.Entity

{
	public class Product
	{
		public int ProductId { get; set; }

		public string? DescriptionProduction { get; set; }

		public required string ProductName { get; set; }

		public int Price { get; set; }

        public int SalePrice { get; set; }

        public int Status { get; set; }




    }
}

