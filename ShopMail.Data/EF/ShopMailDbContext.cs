﻿using System;
using Microsoft.EntityFrameworkCore;
using ShopMail.Data.Entity;

namespace ShopMail.Data.EF
{
	public class ShopMailDbContext : DbContext
	{
        public ShopMailDbContext(DbContextOptions options) : base(options)
        {
		}

		public DbSet<Product> products;	
		public DbSet<Category> categories;	
		public DbSet<User> users;	
	}
}

