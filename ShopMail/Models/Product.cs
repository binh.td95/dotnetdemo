﻿using System;
namespace ShopMail.Models
{
	public class Product
	{
        public int ProductId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Descript { get; set; }

        public int Status { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}

